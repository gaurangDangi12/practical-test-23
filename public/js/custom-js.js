$(function() 
{
     var url     = $(location).attr('href'),
    parts       = url.split("/"),
    last_part   = parts[parts.length-1];
    window.setTimeout(function()
    { 
       $(".alert-danger").hide(); 
    },10000);

    /**
    * Login form jquery validation.
    */
    $("form[name='login']").validate
    ({
        // Specify validation rules
        rules: 
        {
            email: 
            {
                required: true,
                email: true
            },
            password: 
            {
                required: true,
            }
        },
        // Specify validation error messages
        messages: 
        {
            email: "Please enter a valid email address.",
            password: 
            {
                required: "Please provide a password.",
            },
        },
        submitHandler: function(form) 
        {
            $("input").prop('disabled',true);
            $(".login-btn").prop('disabled',true);
            form.submit();
        }
    });


    /**
    * Sign up form jquery validation.
    */
    $("form[name='signup']").validate
    ({
        // Specify validation rules
        rules: 
        {
            plant_id: 
            {
                required: true,
                digits: true,
                maxlength : 6
            },
            email: 
            {
                required: true,
                email: true
            },
            password: 
            {
                required: true,
            }
        },
        // Specify validation error messages
        messages: 
        {
            plant_id:
            {
                required: "Please enter a plant id.",
                digits:  "Please enter a digit in plant id.",
                maxlength : "Please enter 6 digit plant id."
            },
            email:
            {
                required: "Please enter a email address.",
                email: "Please enter a valid email address.",
            },
            password: 
            {
                required: "Please enter a password.",
            },
        },
        submitHandler: function(form) 
        {
            $("input").prop('disabled',true);
            $(".login-btn").prop('disabled',true);
            form.submit();
        }
    });

    /**
    * Change password jquery validation and form submit.
    */
    $("form[name='changePasswordForm']").validate
    ({
        // Specify validation rules
        rules: 
        {
            password: 
            {
                required: true,
            },
            confirm_password: 
            {
                required: true,
                equalTo: "#password"
            }
        },
        // Specify validation error messages
        messages: 
        {
            password: 
            {
                required: "Please enter a password.",
            },
            confirm_password: 
            {
                required:  "Please enter a confirm password.",
                equalTo: " Enter Confirm Password Same as Password"
            }
        },
        submitHandler: function(form) 
        {
            $("input").prop('disabled',true);
            $(".change-pwd-btn").prop('disabled',true);
            $.ajax
            ({
                url: changePwdUrl,
                method: "GET",
                data: 
                {
                    password        :$("#password").val(),
                    changePassword  :$("#confirm_password").val(),
                },
                beforeSend: function() 
                {
                    $(".change-pwd-btn .fa.fa-spinner").removeClass('hideClass');
                },
                success: function(data)
                {
                    $(".change-pwd-btn .fa.fa-spinner").addClass('hideClass');
                    if(data.response == 1)
                    {
                        $(".pwdError").text('');
                        $(".pwdError").text(data.message);
                        window.setTimeout(function(){ $('.pwdError').hide(); },1500); 
                        $(".change-pwd-btn").prop('disabled',true);
                    }
                    if(data.response == 2 || data.response == 3)
                    {
                        $(".cpwdError").text('');
                        
                        $(".cpwdError").text(data.message);
                        window.setTimeout(function(){ $('.cpwdError').hide(); },1500); 
                        $(".change-pwd-btn").prop('disabled',true);
                    }
                    else
                    {
                        $("input").prop('disabled',false);
                        $(".change-pwd-btn").prop('disabled',false);
                        $("#password").val("");
                        $("#confirm_password").val("");
                        $("input[type=text], textarea"). val(""); 
                        var html = '<div class="alert alert-primary" role="alert">'+data.message+'</div>';
                        $('.success').show();
                        $('.success').html(html);
                        window.setTimeout(function(){ $('.success').hide(); },2000);    
                    }            
                },
                error: function(jqXHR, textStatus, errorThrown)
                {     
                    $(".change-pwd-btn .fa.fa-spinner").addClass('hideClass');
                    console.log(textStatus, errorThrown);
                }
            });
        }
    });


    /**
    * Delete jquery validation and form submit.
    */
    $(".delete-confirmation-btn").click(function()
    {
        $(".delete-confirmation-btn").prop('disabled',true);
        $(".cancel-cstm-btn").prop('disabled',true);   
        if($("#accountAassword").val() == "")
        {
            $(".pwdError").html("Please enter your account password.");
            $(".delete-confirmation-btn").prop('disabled',false);
            $(".cancel-cstm-btn").prop('disabled',false);
            return false;
        }
        else
        {
            $.ajax
            ({
                url         : deleteActUrl,
                method      : "POST",
                headers: 
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data        : 
                {
                    password : $("#accountAassword").val()
                },
                beforeSend  : function() 
                {
                    $(".delete-confirmation-btn .fa.fa-spinner").removeClass('hideClass');
                },
                success     : function(data)
                {
                    $(".delete-confirmation-btn .fa.fa-spinner").addClass('hideClass');
                    if(data.success == 0)
                    {
                        $(".pwdError").html(data.message);
                        $(".delete-confirmation-btn").prop('disabled',false);
                        $(".cancel-cstm-btn").prop('disabled',false);
                        return false;             
                    }
                    if(data.success == 1)
                    {
                        $(".text-message-custom-class").text(data.message);
                        $('#delete-confirmation-modal').modal('hide');
                        $('#confirmation-modal').modal('show');
                        window.setTimeout(function()
                        { 
                           $(location).attr('href',logoutUrl); 
                        },2000); 
                    }                            
                },
                error       : function(jqXHR, textStatus, errorThrown)
                {     
                    $(".change-pwd-btn .fa.fa-spinner").addClass('hideClass');
                    console.log(textStatus, errorThrown);
                }
            });    
        }

    })


    /**
    * Change password jquery validation and form submit.
    */
    $("form[name='pricePerkWhForm']").validate
    ({
        // Specify validation rules
        rules: 
        {
            pricePerkWh: 
            {
                required: true,
            }
        },
        // Specify validation error messages
        messages: 
        {
            pricePerkWh: 
            {
                required: "Please enter a Price.",
            },
        },
        submitHandler: function(form) 
        {
            $("input").prop('disabled',true);
            $(".price-Per-kWh-btn").prop('disabled',true);
            $.ajax
            ({
                url: updateKwhUrl,
                method: "GET",
                data: 
                {
                    pricePerkWh        :$("#pricePerkWh").val(),
                },
                beforeSend: function() 
                {
                    $(".price-Per-kWh-btn .fa.fa-spinner").removeClass('hideClass');
                },
                success: function(data)
                {
                    $(".price-Per-kWh-btn .fa.fa-spinner").addClass('hideClass');
                    if(data.success == 1)
                    {
                        $("input").prop('disabled',false);
                        $(".price-Per-kWh-btn").prop('disabled',false);
                        var html = '<div class="alert alert-primary" role="alert">'+data.message+'</div>';
                        $('.success').show();
                        $('.success').html(html);
                        window.setTimeout(function(){ $('.success').hide(); },2000);
                    }
                              
                },
                error: function(jqXHR, textStatus, errorThrown)
                {     
                    $(".price-Per-kWh-btn .fa.fa-spinner").addClass('hideClass');
                    console.log(textStatus, errorThrown);
                }
            });
        }
    });

    $('.count,.counter-1,.counter-2').each(function () 
    {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.abs(now).toFixed(2));
            }
        });
    });
    $('.counter-2-2').each(function () 
    {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.round(now));
            }
        });
    });
    if(last_part=="livedata")
    {
        lineChartGraph(PowerInWatt,"Power in Watt",time);
    }


/* Start code for change radio button event. */
    $("input[name=power_data]:radio").change(function () 
    {
        var selectedOptions = $("input[name=power_data]:checked").val();
        var html = '<div class="center"><i class="fa fa-spinner fa-spin"></i> Please Wait....</div>';
        $(".chart-data-main-class").html("");
        $(".chart-data-main-class").append(html);

        var graphHtml   = '<canvas id="line-chart"></canvas>';
            $.ajax
            ({
                url    : liveDataGraphUpdate,
                method : "GET",
                data: 
                {
                    selectedOptions : selectedOptions,
                },
                beforeSend: function() 
                {
                },
                success: function(data)
                {
                    $(".chart-data-main-class").html("");
                    $(".chart-data-main-class").append(graphHtml);
                    
                    if(data.success == 1)
                    {
                        var html = '<div class="alert alert-primary" role="alert">'+data.message+'</div>';
                        $('.success').show();
                        $('.success').html(html);
                        lineChartGraph(data.value,data.labelTextValue,data.time);
                        window.setTimeout(function(){ $('.success').hide(); },2000);
                    }
                              
                },
                error: function(jqXHR, textStatus, errorThrown)
                {     
                    $(".price-Per-kWh-btn .fa.fa-spinner").addClass('hideClass');
                    console.log(textStatus, errorThrown);
                }
            });
    });
/* End code for change radio button event. */
/* Start code for get live chart data button event. */
    function lineChartGraph(dataValue,labelTextValue,time="")
    {
        var speedCanvas = document.getElementById("line-chart");
        Chart.defaults.global.defaultFontFamily = "Lato";
        Chart.defaults.global.defaultFontSize = 15;

        var speedData = 
        {
            labels: [time],
            datasets: 
            [{
                label: labelTextValue,
                data: [dataValue],
            }]
        };

        var chartOptions = 
        {
            responsive: true,
            legend: 
            {
                display: true,
                position: 'top',
                labels: 
                {
                    boxWidth: 80,
                    fontColor: 'black'
                }
            },
        };

        var lineChart = new Chart(speedCanvas, 
        {
            type: 'bar',
            data: speedData,
            options: chartOptions
        });
    }
/* End code for get live chart data button event. */
/* Start code for get live chart data button event. */
    /**
    * Delete jquery validation and form submit.
    */
    $(".refresh-data-btn").click(function()
    {
        refreshDataPageFunction()

    })
    function refreshDataPageFunction()
    {
        console.log("Test.......");
        $(".refresh-data-btn").prop('disabled',true);
        $(".refresh-data-btn .fa.fa-spinner").removeClass('hideClass');
        $.ajax
        ({
            url: refreshDaturlUpdate,
            method: "GET",
            data: 
            {},
            beforeSend: function() 
            {
                
            },
            success: function(data)
            {
                $(".refresh-data-btn").prop('disabled',false);
                $(".refresh-data-btn .fa.fa-spinner").addClass('hideClass');
                if(data.success == 1)
                {
                    var html = '<div class="alert alert-primary" role="alert">'+data.message+'</div>';
                    $('.success-msg').show();
                    $('.success-msg').html(html);
                    $(".cstm-grph-details-class").html(data.viewFile);
                    window.setTimeout(function(){ $('.success-msg').hide(); },2000);
                }                            
            },
            error: function(jqXHR, textStatus, errorThrown)
            {     
                $(".refresh-data-btn .fa.fa-spinner").addClass('hideClass');
                console.log(textStatus, errorThrown);
            }
        });
    }
    if(last_part=="livedata")
    {
        if(rfrshmlscnd != 0)
        {
            setInterval(function()
            { 
                var d = new Date();
                var n = d.getSeconds();
                console.log(n);
                refreshDataPageFunction() ;
            }, rfrshmlscnd);
        }        
    }
/* End code for get live chart data button event. */

/* Start code for get live chart home page. */
    if(last_part == "home")
    {
        homePageLineChartGraph();
    }
    function homePageLineChartGraph()
    {

        var html = '<div class="center"><i class="fa fa-spinner fa-spin"></i> Please Wait....</div>';
        $(".chart-data-main-class-home").html("");
        $(".chart-data-main-class-home").append(html);

        var request = $.ajax
        ({
            url: gethomepagegrpahURL,
            method: "GET",
            data:
            {
            },
            beforeSend: function()
            {
                //$(".loadmpre_loader").css('display','flex');
            },            
        });

        request.done(function (data) 
        {
            var graphHtml   = '<canvas id="line-chart-1"></canvas>';
            $(".chart-data-main-class-home").html("");
            $(".chart-data-main-class-home").append(graphHtml);
            var chartData   = data['chart'][0];
            var badgesData  = data['badges'][0];
            var speedCanvas = document.getElementById("line-chart-1");
            Chart.defaults.global.defaultFontFamily = "Lato";
            Chart.defaults.global.defaultFontSize = 15;
            var dataset =  
            {
                labels: chartData['Time'],
                datasets: [
                    {
                        type: 'line',
                        label: 'Power',
                        data: chartData['Power'],
                        backgroundColor: 'rgba(255, 99, 132, 0.2)'

                    }
                ]
            };
            var chartOptions = 
            {
                responsive: true,
                plugins: {
                  title: {
                    display: true,
                    text: 'Chart.js Line Chart - Multi Axis'
                  }
                },
                legend: 
                {
                    display: true,
                    position: 'top',
                    labels: 
                    {
                        boxWidth: 10,
                        fontColor: 'black'
                    },

                }
            };

            var lineChart = new Chart(speedCanvas, 
            {
                type: 'line',
                data: dataset,
                options: chartOptions
            });
        });

        /*var speedCanvas = document.getElementById("line-chart-1");
        Chart.defaults.global.defaultFontFamily = "Lato";
        Chart.defaults.global.defaultFontSize = 15;

        var speedData = 
        {
            labels: ["0","10","20"],
            datasets: 
            [
                {
                    label: "Watt",
                    data: [power1],
                    backgroundColor: "lightgray",
                },
            ]
        };

        var chartOptions = 
        {
            responsive: true,
            legend: 
            {
                display: true,
                position: 'top',
                labels: 
                {
                    boxWidth: 80,
                    fontColor: 'black'
                },

            }
        };

        var lineChart = new Chart(speedCanvas, 
        {
            type: 'line',
            data: speedData,
            options: chartOptions
        });*/
    }
/* End code for get live chart data home page. */
    
    /* Admin Section */
        /**
        * Change password jquery validation and form submit.
        */
        $("form[name='RefreshDataInfoForm']").validate
        ({
            // Specify validation rules
            rules: 
            {
                refresh_live_data: 
                {
                    required: true
                },
            },
            // Specify validation error messages
            messages: 
            {
                refresh_live_data: 
                {
                    required: "Please enter a value.",
                }
            },
            submitHandler: function(form) 
            {
                $("input").prop('disabled',true);
                $(".refresh-data-btn-scnd").prop('disabled',true);
                $.ajax
                ({
                    url: changeRfrshDataUrl,
                    method: "GET",
                    data: 
                    {
                        data        :$("#refresh_live_data").val(),
                    },
                    beforeSend: function() 
                    {
                        $(".refresh-data-btn-scnd .fa.fa-spinner").removeClass('hideClass');
                    },
                    success: function(data)
                    {
                        $(".refresh-data-btn-scnd .fa.fa-spinner").addClass('hideClass');
                        if(data.response == 1)
                        {
                            $("input").prop('disabled',false);
                            $(".refresh-data-btn-scnd").prop('disabled',false);
                            var html = '<div class="alert alert-primary" role="alert">'+data.message+'</div>';
                            $('.success-msg').show();
                            $('.success-msg').html(html);
                            window.setTimeout(function(){ $('.success-msg').hide(); },2000);    
                        }            
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {     
                        $(".change-pwd-btn .fa.fa-spinner").addClass('hideClass');
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        });
    /* Admin Section */
    $('#example').DataTable({
        "ordering": false,
        responsive: true,
         rowReorder: {
            selector: 'td:nth-child(2)'
        },
    });


    /**
    * Delete jquery validation and form submit.
    */
    $(".approved-confirmation-btn").click(function()
    {
        $(".delete-confirmation-btn").prop('disabled',true);
        $(".cancel-cstm-btn").prop('disabled',true);   
        $.ajax
        ({
            url         : approvedUserAccnURL,
            method      : "POST",
            headers: 
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data        : 
            {
                userid : $(".useridclass").val()
            },
            beforeSend  : function() 
            {
                
            },
            success     : function(data)
            {
                if(data.success == 1)
                {
                    $(".text-message-custom-class").text(data.message);
                    $("#approved-confirmation-modal").modal('hide');
                    $('#confirmation-modal').modal('show');
                    window.setTimeout(function()
                    { 
                       location.reload();
                    },2000); 
                }                            
            },
            error       : function(jqXHR, textStatus, errorThrown)
            {     
                console.log(textStatus, errorThrown);
            }
        });
    })
    /* Start code for historical data . */
        if(last_part == "historydata")
        {
            var selectedDaysOptions = $("input[name=days_data]:checked").val();
            getReportData(selectedDaysOptions);
        }
        
        function getReportData(params) 
        {
            var html = '<div class="center"><i class="fa fa-spinner fa-spin"></i> Please Wait....</div>';
            $(".chart-data-main-class-hstry").html("");
            $(".chart-data-main-class-hstry").append(html);
            var request = $.ajax
            ({
                url: gethstChrtURL,
                method: "GET",
                data:
                {
                  param:params  
                },
                beforeSend: function()
                {
                    //$(".loadmpre_loader").css('display','flex');
                },            
            });

            request.done(function (data) 
            {
                var graphHtml   = '<canvas id="line-chart-data-hst"></canvas>';
                $(".chart-data-main-class-hstry").html("");
                $(".chart-data-main-class-hstry").append(graphHtml);
                var chartData = data['chart'][0];
                var badgesData = data['badges'][0];
                //setDataToBadges(badgesData);
                var speedCanvas = document.getElementById("line-chart-data-hst");
                Chart.defaults.global.defaultFontFamily = "Lato";
                Chart.defaults.global.defaultFontSize = 15;
                var dataset =  
                {
                    labels: chartData['Time'],
                    datasets: [
                        {
                            label: 'Power',
                            data: chartData['Power'],
                            backgroundColor: 'rgba(255, 99, 132, 0.2)'
                        }
                    ]
                };
                var chartOptions = 
                {
                    responsive: true,
                    plugins: {
                      title: {
                        display: true,
                        text: 'Chart.js Line Chart - Multi Axis'
                      }
                    },
                    legend: 
                    {
                        display: true,
                        position: 'top',
                        labels: 
                        {
                            boxWidth: 10,
                            fontColor: 'black'
                        },

                    }
                };

                var lineChart = new Chart(speedCanvas, 
                {
                    type: 'line',
                    data: dataset,
                    options: chartOptions
                });
                lineChart.ctx.canvas.addEventListener('wheel', lineChart._wheelHandler);
            });
        }
    /* End code for historical data . */
    /* Start code for change radio button event for historical data. */
        $("input[name=historical_info]:radio").change(function () 
        {
            //Historical data
            var selectedOptions = $("input[name=historical_info]:checked").val();
            //Days selection.
            var selectedDaysOptions = $("input[name=days_data]:checked").val();
            showHistoricalGraph(selectedOptions,selectedDaysOptions)
        });

        $("input[name=days_data]:radio").change(function () 
        {
            //Historical data
            var selectedOptions = $("input[name=historical_info]:checked").val();
            //Days selection.
            var selectedDaysOptions = $("input[name=days_data]:checked").val();

            if(selectedOptions == "power_production")
            {
                getReportData(selectedDaysOptions);
            }
            else
            {
                showHistoricalGraph(selectedOptions,selectedDaysOptions)
            }
        });

        function showHistoricalGraph(selectedOptions,selectedDaysOptions)
        {
            var html = '<div class="center"><i class="fa fa-spinner fa-spin"></i> Please Wait....</div>';
            $(".chart-data-main-class-hstry").html("");
            $(".chart-data-main-class-hstry").append(html);
            var graphType = "line";
            if(selectedOptions == "Today")
                graphType = "bar";
            var graphHtml   = '<canvas id="line-chart-data-hst"></canvas>';
            $.ajax
            ({
                url    : gethstChrtInfoURL,
                method : "GET",
                data: 
                {
                    param : selectedOptions,
                    daysP : selectedDaysOptions
                },
                beforeSend: function() 
                {
                },
                success: function(data)
                {
                    $(".chart-data-main-class-hstry").html("");
                    $(".chart-data-main-class-hstry").append(graphHtml);
                    
                    var chartData = data['chart'][0];
                    var badgesData = data['badges'][0];
                    $(".date-class").html(data['date']);
                    var speedCanvas = document.getElementById("line-chart-data-hst");
                    Chart.defaults.global.defaultFontFamily = "Lato";
                    Chart.defaults.global.defaultFontSize = 15;
                    var dataset =  
                    {
                        labels: chartData['Time'],
                        datasets: 
                        [
                            {
                                label: selectedOptions,
                                data: chartData[selectedOptions],
                                backgroundColor: 'rgb(68, 166, 154)'
                            }
                        ]
                    };
                    var chartOptions = 
                    {
                        responsive: true,
                        plugins: 
                        {
                            title: 
                            {
                                display: true,
                                text: 'Chart.js Line Chart - Multi Axis'
                            },
                        },
                        legend: 
                        {
                            display: true,
                            position: 'top',
                            labels: 
                            {
                                boxWidth: 10,
                                fontColor: 'black'
                            },

                        }
                    };

                    var lineChart = new Chart(speedCanvas, 
                    {
                        type: graphType,
                        data: dataset,
                        options: chartOptions
                    });    
                    lineChart.ctx.canvas.addEventListener('wheel', lineChart._wheelHandler);    
                },
                error: function(jqXHR, textStatus, errorThrown)
                {     
                    $(".price-Per-kWh-btn .fa.fa-spinner").addClass('hideClass');
                    console.log(textStatus, errorThrown);
                }
            });
        }

        $(".export-data-btn").click(function()
        {
            $(".export-data-btn").prop('disabled',true);
            $(".export-data-btn .fa.fa-spinner").removeClass('hideClass');

            //Historical data
            var selectedOptions = $("input[name=historical_info]:checked").val();
            //Days selection.
            var selectedDaysOptions = $("input[name=days_data]:checked").val();

            window.location = exporthistoricalURL+"?hstricalOptions="+selectedOptions+"&days="+selectedDaysOptions;
            $(".export-data-btn").prop('disabled',false);
            $(".export-data-btn .fa.fa-spinner").addClass('hideClass');
        });
    /* End code for change radio button event for historical data. */

    /* Start */
        if(last_part=="daily-monthly-overview")
        {
            //Historical data
            var selectedOptions = $("input[name=daily_month_chart]:checked").val();
            dailyMonthGraph(selectedOptions);
        }

        $("input[name=daily_month_chart]:radio").change(function () 
        {
            //Historical data
            var selectedOptions = $("input[name=daily_month_chart]:checked").val();
            dailyMonthGraph(selectedOptions);
        });
        function dailyMonthGraph(selectedOptions)
        {
            var html = '<div class="center"><i class="fa fa-spinner fa-spin"></i> Please Wait....</div>';
            $(".chart-monthly-daily-data-main-class").html("");
            $(".chart-monthly-daily-data-main-class").append(html);

            var request = $.ajax
            ({
                url: getDailyMonthlyUrl,
                method: "GET",
                data:
                {
                  param:selectedOptions  
                },
                beforeSend: function()
                {
                    //$(".loadmpre_loader").css('display','flex');
                },            
            });

            request.done(function (data) 
            {
                var graphHtml   = '<canvas id="bar-chart"></canvas>';
                $(".chart-monthly-daily-data-main-class").html("");
                $(".chart-monthly-daily-data-main-class").append(graphHtml);
                var chartData = data['chart'][0];
                var badgesData = data['badges'][0];
                
                if(selectedOptions == "monthly")
                    $(".date-class").html("Monthly");
                else    
                    $(".date-class").html(data['time']);
                
                var speedCanvas = document.getElementById("bar-chart");
                Chart.defaults.global.defaultFontFamily = "Lato";
                Chart.defaults.global.defaultFontSize = 15;

                var speedData = 
                {
                    labels: chartData['Month'],
                    datasets: 
                    [
                        {
                            label: "Power Today",
                            data: chartData['Power Today'],
                            backgroundColor: 'rgba(255, 99, 132, 0.2)'
                        },
                        {
                            label: "Power Total",
                            data: chartData['Power Total'],
                            backgroundColor: 'rgba(54, 162, 235, 0.2)'
                        },
                    ]
                };

                var chartOptions = 
                {
                    responsive: true,
                    legend: 
                    {
                        display: true,
                        position: 'top',
                        labels: 
                        {
                            boxWidth: 80,
                            fontColor: 'black'
                        }
                    },
                    gridLines: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,

                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                };

                var lineChart = new Chart(speedCanvas, 
                {
                    type: 'bar',
                    data: speedData,
                    options: chartOptions
                });

            });
        }
        $(".export-graph-data-btn").click(function()
        {
            $(".export-graph-data-btn").prop('disabled',true);
            $(".export-graph-data-btn .fa.fa-spinner").removeClass('hideClass');

            var selectedOptions = $("input[name=daily_month_chart]:checked").val();

            window.location = exportDailyURL+"?param="+selectedOptions;
            $(".export-graph-data-btn").prop('disabled',false);
            $(".export-graph-data-btn .fa.fa-spinner").addClass('hideClass');
        });
    /* End */
});