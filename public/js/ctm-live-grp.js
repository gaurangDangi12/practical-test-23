 /* Start code for change radio button event. */
    $("input[name=power_data]:radio").change(function () 
    {
        var selectedOptions = $("input[name=power_data]:checked").val();
        var html = '<div class="center"><i class="fa fa-spinner fa-spin"></i> Please Wait....</div>';
        $(".chart-data-main-class").html("");
        $(".chart-data-main-class").append(html);

        var graphHtml   = '<canvas id="line-chart"></canvas>';
            $.ajax
            ({
                url    : liveDataGraphUpdate,
                method : "GET",
                data: 
                {
                    selectedOptions : selectedOptions,
                },
                beforeSend: function() 
                {
                },
                success: function(data)
                {
                    $(".chart-data-main-class").html("");
                    $(".chart-data-main-class").append(graphHtml);
                    
                    if(data.success == 1)
                    {
                        var html = '<div class="alert alert-primary" role="alert">'+data.message+'</div>';
                        $('.success').show();
                        $('.success').html(html);
                        lineChartGraph(data.value,data.labelTextValue);
                        window.setTimeout(function(){ $('.success').hide(); },2000);
                    }
                              
                },
                error: function(jqXHR, textStatus, errorThrown)
                {     
                    $(".price-Per-kWh-btn .fa.fa-spinner").addClass('hideClass');
                    console.log(textStatus, errorThrown);
                }
            });
    });
/* End code for change radio button event. */