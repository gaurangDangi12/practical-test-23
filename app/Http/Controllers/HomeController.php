<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Console\Presets\Bootstrap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Mail;
use Config;
use App\User;
use App\JobApplicant;
use Illuminate\Support\HtmlString;

class HomeController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        //Get Second dynamic.
    }
	public function index()
	{
		$title = "Sign Up";
        return view('register',compact('title'));  
	} 
	/**
	* Job Application Form
	*/
	public function applicant()
	{
		$userApplicant 	=  JobApplicant::get();
		$title 			= "Job List Applicant";
        return view('applicant',compact('title','userApplicant'));  
	}
	/**
	* Job Application Form Submit
	*/
	public function jobApplicationRegister(Request $request )
	{
		$data           = $request->input();
        $validatedData  = $request->validate
        ([
            'name'      		=> 'required|min:6|max:25',
            'email'     		=> 'required|string|email|max:255|unique:job_applicant,email',
            'gender'    		=> 'required',
            'ssc_board'    		=> 'required',
            'ssc_year'    		=> 'required|numeric',
            'ssc_Percentage'    => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'degree_board'    	=> 'required',
            'degree_year'    	=> 'required|numeric',
            'degree_Percentage' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'prefered_location' => 'required',
            'currentctc'    	=> 'required|numeric',
            'expectedctc'    	=> 'required|numeric',
            'noticeperiod'    	=> 'required',
            'companyname' 		=> 'required',
            'designation'    	=> 'required',
            'from_date'    		=> 'required',
            'to_date'    		=> 'required',
        ]);
        if (!$validatedData) 
        {
            flash("Something went wrong")->warning();
            return back();
        }
        if (!$this->create($data)) 
        { 
            flash("Something went wrong")->warning();
            return back();
        }
        flash("Job Application Register Successfully.");
        return redirect('/login');
	}
	/**
	* Job Application Form Info store in db
	*/
	public function create($data)
    {
    	return JobApplicant::create
        ([
            'name'                  => trim($data['name']),
            'email'                 => trim($data['email']),
            'gender'                => trim($data['gender']),
            'ssc_board'             => trim($data['ssc_board']),
            'ssc_year'              => trim($data['ssc_year']),
            'scc_percentage'        => trim($data['ssc_Percentage']),
            'hsc_board'             => trim($data['hsc_board']),
            'hsc_year'              => trim($data['hsc_year']),
            'hsc_percentage'        => trim($data['hsc_Percentage']),
            'degree_board'          => trim($data['degree_board']),
            'degree_year'           => trim($data['degree_year']),
            'degree_percantage'     => trim($data['degree_Percentage']),            
            'prefered_location'     => trim($data['prefered_location']),            
            'currentctc'     		=> trim($data['currentctc']),            
            'expectedctc'     		=> trim($data['expectedctc']),            
            'noticeperiod'     		=> trim($data['noticeperiod']), 
		    'companyname'     		=> trim($data['companyname']),            
            'designation'     		=> trim($data['designation']),            
            'from_date'     		=> date('Y-m-d',strtotime(trim($data['from_date']))),            
            'to_date'     			=> date('Y-m-d',strtotime(trim($data['to_date']))),            
        ]);
    }
    /**
    * Delete Applicant
    */
    public function deleteApplicant($id)
    {
    	$userId = JobApplicant::where('id', $id)->delete();
    	flash("Job Application Deleted Successfully.");
        return redirect('/applicant-list');
    }
}

