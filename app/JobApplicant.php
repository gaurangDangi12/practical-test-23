<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class JobApplicant extends Authenticatable
{
    
    use Notifiable;

    protected $table        = 'job_applicant';

   
    protected $fillable = 
    [
        'name', 'email','gender','ssc_board', 'ssc_year','scc_percentage','hsc_board', 'hsc_year','hsc_percentage','degree_board', 'degree_year','degree_percantage','prefered_location','currentctc','expectedctc','noticeperiod','companyname','designation','from_date','to_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
