<?php

namespace Illuminate\Foundation\Auth;

trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        $userStatus = \Illuminate\Support\Facades\Auth::user()['is_status'];
        if($userStatus == 0)
        {
            $this->guard()->logout();
            flash('Your account is blocked please contact Administrator.')->error();
            $this->redirectTo = "/login";
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/login';
        }
        else if($userStatus == 2)
        {
            $this->guard()->logout();
            flash('Your account is deleted please contact Administrator.')->error();
            $this->redirectTo = "/login";
           return property_exists($this, 'redirectTo') ? $this->redirectTo : '/login';
        }
        else
        {
            if (method_exists($this, 'redirectTo')) {
                return $this->redirectTo();
            }

            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
        }
    }
}
