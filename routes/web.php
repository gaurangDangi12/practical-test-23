<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('vendorreport/{client_id}/{vendor_id}/{month}/{year}', 'qualificationController@report');
Route::get('/', function () {
    return redirect('/login');
}); 
/* Code for execute scripts... */

 Artisan::call('cache:clear');
/* Code for execute scripts... */
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('jobapplication', 'HomeController@index');
Route::get('applicant-list', 'HomeController@applicant');
Route::get('delete-application/{id}', 'HomeController@deleteApplicant');
Route::post('job-application-register', 'HomeController@jobApplicationRegister')->name('jobApplicationRegister');
Auth::routes();