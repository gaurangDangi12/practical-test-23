<?php

return [
     
     'CONTENT_TYPE'                     => 'application/json',
     'WEBSITE_TITLE'                    => 'Sebotec Energy',
     'CONTACT_NO_HEADER'                => '+494627-3449967',
     'CONTACT_NO_FOOTER'                => '+494627-344-9967',
     'CONTACT_EMAIL'                    => 'info@sebotec.de',
     'CONTACT_ADDRESS'                  => 'House- 65/4, Zonson street-3/5, London, UK',
];
