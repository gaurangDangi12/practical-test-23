<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- favicon -->        
        <title>Job Application</title>
        <!-- all css here -->
        <!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css') }}">
        <!-- owl.carousel css -->
        <link rel="stylesheet" href="{{ url('/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ url('/css/owl.transitions.css') }}">
       <!-- Animate css -->
        <link rel="stylesheet" href="{{ url('/css/animate.css') }}">
        <!-- meanmenu css -->
        <link rel="stylesheet" href="{{ url('/css/meanmenu.min.css') }}">
        <!-- font-awesome css -->
        <link rel="stylesheet" href="{{ url('/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/themify-icons.css') }}">
        <link rel="stylesheet" href="{{ url('/css/flaticon.css') }}">
        <!-- venobox css -->
        <link rel="stylesheet" href="{{ url('/css/venobox.css') }}">
        <!-- magnific css -->
        <link rel="stylesheet" href="{{ url('/css/magnific.min.css') }}">
        <!-- style css -->
        <link rel="stylesheet" href="{{ url('/style.css') }}">
        <link rel="stylesheet" href="{{ url('/css/custom-css.css') }}">
        <link rel="stylesheet" href="https://webthemez.com/demo/brilliant-free-bootstrap-admin-template/assets/js/Lightweight-Chart/cssCharts.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{ url('/css/responsive.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">
        
        <!-- modernizr css -->
        <script src="{{ url('/js/vendor/modernizr-2.8.3.min.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
    <body>
        <div id="preloader"></div>
        <div class="col-md-9 col-sm-9">
            <!-- mainmenu start -->
            <nav class="navbar navbar-default">
                <div class="collapse navbar-collapse" id="navbar-example">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                            @if(\Illuminate\Support\Facades\Auth::user())
                                <li><a href="{{ url('/applicant-list') }}">Submitted Application</a></li>
                                <li><a href="{{ url('/logout') }}">Logout</a></li>
                                
                                <li></li>
                            @endif
                            @if(!(\Illuminate\Support\Facades\Auth::user()))
                                <li><a class="s-menu" href="{{ url('/jobapplication') }}">Job App</a></li>
                                <li><a class="s-menu" href="{{ url('/login') }}">Login</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- mainmenu end -->
        </div>
        @if(isset($success) && $success!='')
            <div class="CustomAlert alert-success capitalizefont" role="alert" style="margin-top: 10px"> 
                  {{$success}}
            </div>
        @endif        
        @if(isset($error) && $error!='')
            <div class="CustomAlert alert-danger capitalizefont" role="alert">
                {!! $error !!}
            </div>  
        @endif
        
        @yield('content')

        
</body>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="{{ url('/js/bootstrap.min.js') }}"></script>    
<!-- jquery latest version -->
<script src="{{ url('/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ url('/js/bootstrap.min.js') }}"></script>
<!-- owl.carousel js -->
<script src="{{ url('/js/owl.carousel.min.js') }}"></script>
<!-- Counter js -->
<script src="{{ url('/js/jquery.counterup.min.js') }}"></script>
<!-- waypoint js -->
<script src="{{ url('/js/waypoints.js') }}"></script>
<!-- magnific js -->
<script src="{{ url('/js/magnific.min.js') }}"></script>
<!-- wow js -->
<script src="{{ url('/js/wow.min.js') }}"></script>
 <!-- venobox js -->
<script src="{{ url('/js/venobox.min.js') }}"></script>
<!-- meanmenu js -->
<script src="{{ url('/js/jquery.meanmenu.js') }}"></script>
<!-- Form validator js -->
<script src="{{ url('/js/form-validator.min.js') }}"></script>
<!-- plugins js -->
<script src="{{ url('/js/plugins.js') }}"></script>
<!-- main js -->
<script src="{{ url('/js/main.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() 
    {
        function approveuserid(userId)
        {
            alert(userId);
        }
    });

</script>
@yield('pageBottomScriptSection')
@yield('editProjectReport')
</html>
