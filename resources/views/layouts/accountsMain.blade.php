<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <link rel="icon" type="image/png" href="{{ URL::to('/') }}/img/fav_icn.png">  
    <link rel="stylesheet" href="{{ mix('/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('/css/glyphicons.css') }}">
    <link rel="stylesheet" href="{{ url('/css/bootstrap-editable.css') }}">
    <link rel="stylesheet" href="{{ url('/css/notify.css') }}">
    <title>Eliteopinio</title>        
    <style>
            table td
            {
                padding: 5px;
                height: 50px;
            }
            table th
            {
                padding: 5px;
            }
    
            body {
                color: #333333;
                font-family: "Open Sans", sans-serif;
                font-size: 13px;
            }
        </style>
    <script type="text/javascript">
        <?php
        header("Cache-Control: no-store, must-revalidate, max-age=0");
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        echo time();
        ?>
    </script>
</head>


<body class="bg-white" style="min-height: 100%">
<header>

    <nav class="navbar navbar-expand-lg navbar-dark deep-purple mb-2">
        <a class="navbar-brand" href="{{url('/accounts/invoice/list')}}"><strong>EliteOpinio</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse accountListClass" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{Request::is('accounts/invoice/list*') ? 'active' : ''}}">
                    <a class="nav-link" href="/accounts/invoice/list">Projects list<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{Request::is('accounts/invoice/idreceived*') ? 'active' : ''}}">
                    <a class="nav-link " href="/accounts/invoice/idreceived">ID Received</a>
                </li>
                <li class="nav-item {{Request::is('accounts/invoice/nocompletes*') ? 'active' : ''}}">
                    <a class="nav-link " href="/accounts/invoice/nocompletes">No completes</a>
                </li>
                <li class="nav-item {{Request::is('accounts/invoice/generated*') ? 'active' : ''}}">
                    <a class="nav-link" href="/accounts/invoice/generated">Generated invoices</a>
                </li>
                <li class="nav-item {{Request::is('accounts/invoice/sentInvoice*') ? 'active' : ''}}">
                    <a class="nav-link" href="/accounts/invoice/sentInvoices">Sent invoices</a>
                </li>
                <li class="nav-item {{Request::is('accounts/invoice/report*') ? 'active' : ''}}">
                    <a class="nav-link" href="/accounts/invoice/report">Invoice report</a>
                </li>
                <li class="nav-item {{Request::is('accounts/invoice/vendorReports*') ? 'active' : ''}}">
                    <a class="nav-link" href="/accounts/invoice/vendorReports">Vendor invoices</a>
                </li>
                @if(\Illuminate\Support\Facades\Auth::user()['role_id'] == 3)
                    <li class="nav-item {{Request::is('users/*') ? 'active' : ''}}">
                        <a class="nav-link" href="/users/list">Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/projects/list">Switch to Surveys</a>
                    </li>
                @endif
            </ul>
             <ul class="navbar-nav nav-flex-icons">
                        <li class="nav-item">
                           
                            <div class="btn-group">
                                <a class="dropdown-toggle text-white" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false"><i
                                            class="fas fa-user mr-2"></i>{{\Illuminate\Support\Facades\Auth::user()['name']}}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="/users/profile">Edit profile</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                           
                        </li>
                    
            </ul>
        </div>
    </nav>

</header>
<main style="min-height: calc(100vh - 91px);">
    <div class="container-fluid ">
        <div class="">
            <div class="w-100 p-2 mb-2 border-bottom row account_page_title" id="page_title">
                <div class="col-3 pl-0 accountTitle">
                    <h4 class="blue-grey-text">{{$pageTitle}}</h4>
                </div>
                <div class="col-9 pr-0">
                    <div class="d-flex justify-content-end pl-2 pr-0 ">
                        @yield('filterBox')
                    </div>
                </div>
                </hr>
            </div>
            <div class="w-100 p-2 mb-2 row">
                <div class="col-12 pr-0 accountFiltrDiv">
                    @php
                        $findString = "invoice/addInvoice";
                        $stringUrl  = Request::fullUrl();
                        $pos = strpos($stringUrl, $findString);
                        /* */
                        $findString1 = "/edit";
                        $stringUrl  = Request::fullUrl();
                        $pos1 = strpos($stringUrl, $findString1);

                        $findString2 = "invoice/report";
                        $stringUrl  = Request::fullUrl();
                        $pos2 = strpos($stringUrl, $findString2);

                    @endphp
                    @if($pos == '' && $pos1 == '' && $pos2 == '' && Request::fullUrl() != url('users/profile') )
                        <div class="prjctFilterLoader"><img src="{{ url('img/loader.gif') }}"></div>
                    @endif
                    
                    <div class="d-flex justify-content-end pl-2 pr-0 ">
                        @yield('searchOptions')
                    </div>
                </div>
            </div>
            @include('flash::message')
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="mt-2 shadow-lg p-1">
                @yield('content')
            </div>
        </div>

    </div>

</main>
<!-- Invoice Delete/Cancelled Confirmation Popup End  -->
    <div class="modal fade InvoiceAlertModal" id="InvoiceAlertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="loaderdiv">
                    <img src="{{ url('img/loader.gif') }}">
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="InvoiceTextTitle">Alert</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modalBody">
                    <input type="hidden" class="invoiceUrl" value="">
                    <strong>Warning!</strong> Are you sure,you want to <p class="prjctNameCls"></p> the invoice?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">No</button>
                    <button type="submit" class="btn btn-primary text-white btnYesE" >Yes</button> 
                </div>
            </div>
        </div>
    </div>
<!-- Invoice Delete/Cancelled Confirmation Popup End -->
<!-- Start Modal for Show Broadcast message if added new -->
    <div class="modal fade BroadcastMessageShowModal" id="BroadcastMessageShowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Notification</h5>
                </div>
                <div class="modal-body modalBody">
                    <p class="BroadCastMessageP"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary text-white" onclick="ChangeBroadcastMessageFlag({{\Illuminate\Support\Facades\Auth::user()['id']}})">Close</button> 
                </div>
            </div>
        </div>
    </div>
<!-- End Modal for Show Broadcast message if added new -->
<footer class="page-footer font-small deep-purple text-white mt-2" style=" width: 100%; ">
    @yield('footerContent')

    <div class="footer-copyright  text-center">
        © {{ date('Y') }} Copyright:
        <a href="{{url('/')}}" class="text-white"> EliteOpinio.com</a>
    </div>
    <!--/.Copyright-->

</footer>
</body>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  
  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="{{ url('js/bootstrap-editable.min.js') }}"></script>
<script src="{{ mix('/js/script.js') }}"></script>
<script src="{{ url('js/customEliteAccount.js') }}"></script>
<script src=" {{ url('js/notify.js') }}"></script>
<script type="text/javascript">
    var getPrjctDateInvceInfo   = "{{ url('accounts/invoice/getPrjctDateInvceInfo') }}"; 
    var userIdFlag = "{{ \Illuminate\Support\Facades\Auth::user()['is_show_bmessage']  }}";
    if(userIdFlag!='' && userIdFlag == 0)
    {
        $(window).on('load',function()
        {
            $.ajax
            ({
                url: '/apidata/getBroadCastMessage',
                method: "GET",
                beforeSend: function() 
                {
                    $(".loaderdiv").css('display','flex');
                },
                success: function(data)
                {
                    if(data.success == 1 && data.message !='')
                    {
                        $(".message").val(data.message);
                        $(".BroadCastMessageP").text(data.message);
                        $("#BroadcastMessageShowModal").modal('show');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {     
                    console.log(textStatus, errorThrown);     
                }
            });
            
        });
    }
    if(userIdFlag == 1)
    {
        $("#BroadcastMessageShowModal").modal('hide');
    }
</script>
@yield('pageBottomScriptSection')

</html>
