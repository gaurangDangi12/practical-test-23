<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/css/bootstrap.css') }}">
    {{--<link rel="stylesheet" href="{{ url('/css/mdb.css') }}">--}}

    <link rel="stylesheet" href="{{ url('/css/style.css') }}">

    <script src="{{ url('/js/bootstrap.js') }}"></script>
    <script src="{{ url('/js/mdb.js') }}"></script>


</head>
<body class="bg-white">
<div>

    @yield('content')
</div>

<!-- Scripts -->

</body>
</html>