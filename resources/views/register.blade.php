@extends('layouts.main')

@section('content')

    <!-- Start Slider Area -->
        <div class="login-area area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-12 col-xs-12">
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="login-page">
                            <div class="login-form">
                                <h4 class="login-title text-center">{{$title}}</h4>
                                <div class="row">
                                    <form action="<?php echo route('jobApplicationRegister'); ?>" method="post">
                                        @csrf
                                        <lable>Basic Information</lable>
                                        <hr>
                                        <div class="">
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>Name</label>
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Name" data-error="Please enter your plant id" value="{{ old('name') }}">
                                                 @if ($errors->has('name'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>E-mail Id</label>
                                                <input type="text" id="email" name="email" class="form-control" placeholder="Email" data-error="Please enter your email-id" value="{{ old('email') }}">
                                                 @if ($errors->has('email'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>Gender</label><br>
                                                <input type="radio" id="male" name="gender" value="male">
                                                <label for="male">Male</label><br>
                                                <input type="radio" id="female" name="gender" value="female">
                                                <label for="female">Female</label><br>
                                                 @if ($errors->has('gender'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('gender') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <lable>Education Information</lable>
                                        <hr>
                                        <div class="">
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>SSC</label><br>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="ssc_board" id="ssc_board" class="form-control" placeholder="Board/University" data-error="Please enter your plant id" value="{{ old('ssc_board') }}">
                                                     @if ($errors->has('ssc_board'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('ssc_board') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="ssc_year" id="ssc_year" class="form-control" placeholder="Year" data-error="Please enter your plant id" value="{{ old('name') }}">
                                                     @if ($errors->has('ssc_year'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('ssc_year') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="ssc_Percentage" id="ssc_Percentage" class="form-control" placeholder="CGPA/Percentage" data-error="Please enter your plant id" value="{{ old('ssc_Percentage') }}">
                                                    @if ($errors->has('ssc_Percentage'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('ssc_Percentage') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>HSC</label><br>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="hsc_board" id="hsc_board" class="form-control" placeholder="Board/University" data-error="Please enter your plant id" value="{{ old('hsc_name') }}">
                                                     @if ($errors->has('hsc_board'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('hsc_board') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="hsc_year" id="hsc_year" class="form-control" placeholder="Year" data-error="Please enter your plant id" value="{{ old('hsc_year') }}">
                                                     @if ($errors->has('hsc_year'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('hsc_year') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="hsc_Percentage" id="hsc_Percentage" class="form-control" placeholder="CGPA/Percentage" data-error="Please enter your plant id" value="{{ old('hsc_Percentage') }}">
                                                     @if ($errors->has('hsc_Percentage'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('hsc_Percentage') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>Master Degree</label><br>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="degree_board" id="degree_board" class="form-control" placeholder="Board/University" data-error="Please enter your plant id" value="{{ old('degree_board') }}">
                                                     @if ($errors->has('degree_board'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('degree_board') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="degree_year" id="degree_year" class="form-control" placeholder="Year" data-error="Please enter your plant id" value="{{ old('degree_year') }}">
                                                     @if ($errors->has('degree_year'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('degree_year') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" name="degree_Percentage" id="degree_Percentage" class="form-control" placeholder="CGPA/Percentage" data-error="Please enter your plant id" value="{{ old('degree_Percentage') }}">
                                                     @if ($errors->has('degree_Percentage'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('degree_Percentage') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>                                            
                                        </div>
                                        <lable>Work Experience Information</lable>
                                        <hr>
                                        <div class="">
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>Company Name</label><br>
                                                <input type="text" id="companyname" name="companyname" class="form-control" placeholder="Company Name" data-error="Please enter your Current CTC" value="{{ old('companyname') }}">
                                                @if ($errors->has('companyname'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('companyname') }}</strong>
                                                    </span>
                                                @endif                                         
                                            </div>   
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>Designation</label><br>
                                                <input type="text" id="designation" name="designation" class="form-control" placeholder="Designation" data-error="Please enter your Current CTC" value="{{ old('designation') }}">
                                                @if ($errors->has('designation'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('designation') }}</strong>
                                                    </span>
                                                @endif                                              
                                            </div>   
                                            <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
                                                <label>From Date</label><br>
                                                <input type="date" id="from_date" name="from_date" class="form-control" placeholder="From Date" data-error="Please enter your Current CTC" value="{{ old('from_date') }}">
                                                @if ($errors->has('from_date'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('from_date') }}</strong>
                                                    </span>
                                                @endif                                              
                                            </div>  
                                            <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
                                                <label>To Date</label><br>
                                                <input type="date" id="to_date" name="to_date" class="form-control" placeholder="From Date" data-error="Please enter your Current CTC" value="{{ old('to_date') }}">
                                                @if ($errors->has('to_date'))
                                                    <span class="error help-block">
                                                        <strong>{{ $errors->first('to_date') }}</strong>
                                                    </span>
                                                @endif                                              
                                            </div>                                            
                                        </div>
                                        <lable>Preference Information</lable>
                                        <hr>
                                        <div class="">
                                            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                                <label>Preferred Location</label><br>
                                                <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
                                                    <select class="form-control" aria-label="Default select example" name="prefered_location">
                                                        <option value="surat" selected>Surat</option>
                                                        <option value="ahmedabad" >Ahmedabad</option>
                                                        <option value="rajkot" >Rajkot</option>
                                                        <option value="gandhinagar" >Gandhinagar</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" id="currentctc" name="currentctc" class="form-control" placeholder="Current CTC" data-error="Please enter your Current CTC" value="{{ old('currentctc') }}">
                                                    @if ($errors->has('currentctc'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('currentctc') }}</strong>
                                                        </span>
                                                    @endif
                                                </div> 
                                                <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="text" id="expectedctc" name="expectedctc" class="form-control" placeholder="Expected CTC" data-error="Please enter your Expected CTC" value="{{ old('expectedctc') }}">
                                                    @if ($errors->has('expectedctc'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('expectedctc') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
                                                    <input type="number" step="1" id="noticeperiod" name="noticeperiod" class="form-control" placeholder="Notice Period(In Days)" data-error="Please enter your Notice Period" value="{{ old('noticeperiod') }}">
                                                    @if ($errors->has('noticeperiod'))
                                                        <span class="error help-block">
                                                            <strong>{{ $errors->first('noticeperiod') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>                                               
                                            </div>                                         
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <input type="submit" id="submit" class="slide-btn login-btn" value="Apply For Job"/>
                                        </div> 
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                    </div>
                </div>
             </div>
        </div>
@endsection