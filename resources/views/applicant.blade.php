@extends('layouts.main')

@section('content')
    <!-- Start Slider Area -->
        <div class="login-area">
            <div class="container">
                
                <div id="page-inner">
                    <div class="row">
                        @include('flash::message')
                        <div class="col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body cstm-width-style">
                                    <table id="example" class="display nowrap" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Name</th>
                                                <th>E-Mail</th>
                                                <th>Gender</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            if(count($userApplicant) != 0)
                                            {
                                                $i = 1;
                                                foreach ($userApplicant as $userDtl) 
                                                {
                                        ?>  
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$userDtl['name']}}</td>
                                                        <td>{{$userDtl['email']}}</td>
                                                        <td>{{ucfirst($userDtl['gender'])}}</td>
                                                        <td>
                                                            <a href="{{url('delete-application/'.$userDtl['id'])}}" class="buttonload button-class refresh-data-btn-scnd">Delete</a></td>
                                                    </tr>
                                        <?php
                                                    $i = $i+1;
                                                }
                                            }
                                            else
                                            {
                                        ?> 
                                                <tr>
                                                    <td colspan="3">No data found.</td>
                                                </tr>
                                        <?php        
                                            }
                                        ?>
                                        </tbody>  
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('pageBottomScriptSection')
@endsection
